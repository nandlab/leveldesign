using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    // Speed of the character
    [SerializeField] float speed = 3f;
    
    void Start() {}
    
    void Update()
    {
        Vector3 moveVector = Vector3.zero;

        // Get input and save state in moveVector
        if (Input.GetKey(KeyCode.W)) moveVector.y += 1;
        if (Input.GetKey(KeyCode.A)) moveVector.x -= 1;
        if (Input.GetKey(KeyCode.S)) moveVector.y -= 1;
        if (Input.GetKey(KeyCode.D)) moveVector.x += 1;

        // Normalize vector, so that magnitude for diagonal movement is also 1
        moveVector.Normalize();

        // Frame rate independent movement
        transform.position += speed * Time.deltaTime * moveVector;
    }
}
